/*

  Задание:

    1. Написать конструктор объекта комментария который принимает 3 аргумента
    ( имя, текст сообщения, ссылка на аватаку);

    {
      name: '',
      text: '',
      avatarUrl: '...jpg'
      likes: 0
    }
      + Создать прототип, в котором будет содержаться ссылка на картинку по умлочанию
      + В прототипе должен быть метод который увеличивает счетик лайков

    var myComment1 = new Comment(...);

    2. Создать массив из 4х комментариев.
    var CommentsArray = [myComment1, myComment2...]

    3. Созадть функцию конструктор, которая принимает массив коментариев.
      И выводит каждый из них на страничку.

    <div id="CommentsFeed"></div>

*/

let container = document.getElementById('result');
function incLikes(){
    this.likes = this.likes + 1;
}

var commentProt = {
    incLikes
    // тоже самое что и talk: talk
};
var commentsArray = [];
function Comment(name,text,avatarUrl){
    this.name = name;
    this.text = text;
    this.avatarUrl = avatarUrl === undefined ? avatarUrl = '3456.jpg' : avatarUrl
    this.likes = 0;

    Object.setPrototypeOf(this,commentProt);
    commentsArray.push(this)

}

let comment1 = new Comment('Andrew','same text');
let comment2 = new Comment('Michael','same shit');
let comment3 = new Comment('John','same crap','boom.jpg');
let comment4 = new Comment('Bruce','same trash');



console.log(commentsArray)
function showComments(arr){
    arr.forEach(function(item){
        let commentBlock = document.createElement('div');
        commentBlock.classList.add('one-comment');
        for (var key in item) {
            if (key !== 'likes' && key !== 'incLikes') {
                let oneRow = document.createElement('p');
                oneRow.innerHTML = item[key];
                commentBlock.appendChild(oneRow);
            }
        }
        container.appendChild(commentBlock);
    })
}

window.addEventListener("load", showComments(commentsArray));

