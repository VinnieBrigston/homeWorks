/*

 Задание 1.

 Написать функцию которая будет задавать СЛУЧАЙНЫЙ цвет для фона.
 Каждая перезагрузка страницы будет с новым цветом.
 Для написания используйте функцию на получение случайного целого числа,
 между минимальным и максимальным значением (Приложена снизу задания)

 + Бонус, повесить обработчик на кнопку через метод onClick
 + Бонус, использовать 6-ричную систему исчесления и цвет HEX -> #FFCC00
 + Бонус выводить полученый цвет по центру страницы.
 Необходимо создать блок через createElement задать ему стили через element.style
 и вывести через appendChild или insertBefore


 Необходимые материалы:
 Math.Random (Доки): https://developer.mozilla.org/uk/docs/Web/JavaScript/Reference/Global_Objects/Math/random
 function getRandomIntInclusive(min, max) {
 min = Math.ceil(min);
 max = Math.floor(max);
 return Math.floor(Math.random() * (max - min + 1)) + min;
 }
 __
 Работа с цветом:
 Вариант 1.
 Исользовать element.style.background = 'rgb(r,g,b)';
 где r,g,b случайное число от 0 до 256;

 Вариант 2.
 Исользовать element.style.background = '#RRGGBB';
 где, RR, GG, BB, значние цвета в 16-ричной системе исчесления
 Формирование цвета в вебе: https://ru.wikipedia.org/wiki/%D0%A6%D0%B2%D0%B5%D1%82%D0%B0_HTML
 Перевод в 16-ричную систему исчесления делается при помощи
 метода Number.toString( 16 ) https://www.w3schools.com/jsref/jsref_tostring_number.asp,

 var myNumber = '251'
 myNumber.toString(16) // fb

 */

/*
 Задание 2.

 Сложить в элементе с id App следующую размету HTML:

 <header>
 <a href="http://google.com.ua">
 <img src="https://www.google.com.ua/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png">
 </a>
 <div class="menu">
 <a href="#1"> link 1</a>
 <a href="#1"> link 2</a>
 <a href="#1"> link 3</a>
 </div>
 </header>


 Используя следующие методы для работы:
 getElementById
 createElement
 element.innerText
 element.className
 element.setAttribute
 element.appendChild

 */

/*  Задание 1. */

/* Формирование рандомного числа */
function getRandomIntInclusive(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

/* Выборка контейнера с id app */
var app = document.getElementById('app');

/* Создание контейнера для вывода значения цвета */
var containerForColor = document.createElement('div');

/* Добавление класса контейнеру */
containerForColor.className = 'color-wrap';

/* Добавление стилей для созданного контейнера */
containerForColor.style.display = 'inline-block';
containerForColor.style.fontSize = '48px';
containerForColor.style.userSelect = 'none';
containerForColor.style.marginBottom = '40px';

/* Добавление контейнера для цвета перед контейнером с id app */
document.body.insertBefore(containerForColor,app);

/* Функция смены цвета для body и добавление значения текста в контейнер */
var changeColorBody = function(){
        var red = getRandomIntInclusive(0,256).toString(16);
        var green = getRandomIntInclusive(0,256).toString(16);
        var blue = getRandomIntInclusive(0,256).toString(16);
        var newColor = '#'+red+green+blue;
        document.body.style.backgroundColor=newColor;
        containerForColor.innerHTML = newColor;
};

/* Вешаем вызов функции на клик по окну*/
document.body.addEventListener('click', changeColorBody);



/*  Задание 2. */

/* создание всех необходимых элементов */
var link = document.createElement('a');
var img = document.createElement('img');
var menu = document.createElement('div');
var header = document.createElement('header');

/* Создание массива с нужными значениями href для ссылок в меню */
var hrefs=['#1','#2','#3'];


/* Функция создания ссылки с href из массива и добавление этой ссылки в меню */
function createLink(item){
    var newLink = document.createElement('a');
    newLink.setAttribute('href',item);
    menu.appendChild(newLink);
}

/* Перебор массива с href функцией createLink */
hrefs.forEach(createLink);

/* добавление необходимых атрибутов для элементов */
img.setAttribute('src','https://www.google.com.ua/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png');
link.setAttribute('href','http://google.com.ua');
menu.setAttribute('class','menu');

/* вставка созданных элементов друг в друга */
app.appendChild(header);
header.appendChild(link);
link.appendChild(img);
header.appendChild(menu);

