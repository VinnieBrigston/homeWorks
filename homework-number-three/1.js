/*

  Задание написать простой слайдер.

    Есть массив с картинками из которых должен состоять наш слайдер.
    По умолчанию выводим первый элмемнт нашего слайдера в блок с id='slider'
    ( window.onload = func(){...} // window.addEventListener('load', function(){...}) )
    По клику на PrevSlide/NextSlide показываем предыдущий или следующий сладй соответствено.

    Для этого необходимо написать 4 функции которые будут:

    1. Срабатывать на событие load обьекта window и загружать наш слайд по умолчанию.
    2. RenderImage -> Очищать текущий контент блока #slider. Создавать нашу картинку и через метод аппенд чайлд вставлять её в слайдер.
    3. NextSlide -> По клику на NextSilde передавать currentPosition текущего слайда + 1 в функцию рендера
    4. PrevSlide -> Тоже самое что предыдущий вариант, но на кнопку PrevSlide и передавать currentPosition - 1
      + бонус сделать так что бы при достижении последнего и первого слада вас после кидало на первый и последний слайд соответственно.
      + бонсу анимация появления слайдов через навешивание дополнительных стилей

*/
    /* Решение */
  var OurSliderImages = ['images/cat1.jpg', 'images/cat2.jpg', 'images/cat3.jpg', 'images/cat4.jpg', 'images/cat5.jpg', 'images/cat6.jpg', 'images/cat7.jpg', 'images/cat8.jpg']

  /* Глобальные переменные */
  var currentPosition = 0,
      mainContainer = document.getElementById('slider'),
      prev = document.getElementById('PrevSilde'),
      next = document.getElementById('NextSilde'),
      countImg = OurSliderImages.length;

  /* При загрузке страницы добавляем все изображение на страницу, скрываем их и показываем первое */
  window.addEventListener('load', function () {
    OurSliderImages.forEach(function(item){
      var startImg = document.createElement('img');
      startImg.classList.add('hide-img');
      startImg.setAttribute('src',item);
      mainContainer.appendChild(startImg);
    });
    mainContainer.childNodes[0].classList.add('show-img');
  });

  /* Функция рендеринга изображения. Скрываем все элементы. Открываем с нужным индексом */
  var RenderImage = function(){
    var content = mainContainer.childNodes;
    content.forEach(function(item){
      item.classList.remove('show-img');
    });
    mainContainer.childNodes[currentPosition].classList.add('show-img');
  };

  /* Функция клика по кнопке следующего слайда */
  var NextSlide = function(){
    currentPosition++;
    if(currentPosition < countImg){
      RenderImage();
    }else{
      currentPosition = 0;
      RenderImage();
    }
  };

  /* Функция клика по кнопке предыдущего слайда */
  var PrevSlide = function(){
    currentPosition--;
    if(currentPosition < 0){
      currentPosition = countImg-1;
      RenderImage();
    }else{
      RenderImage();
    }
};

  next.addEventListener('click',NextSlide);
  prev.addEventListener('click',PrevSlide);
