window.addEventListener('load', function () {
    let allItems = document.querySelectorAll('.constructor__item'),// все li
        addTitleForm = document.querySelector('.add-title'),// форма добавления заголовка
        addTextForm = document.querySelector('.add-text'),// форма добавления text инпутов
        addCheckboxForm = document.querySelector('.add-checkbox'),// форма добавления checkbox
        addRadioForm = document.querySelector('.add-radio'),// форма добавления radio
        addDateForm = document.getElementById('addDate'),// форма добавления radio
        addColorForm = document.getElementById('addColor'),// форма добавления radio
        addPasswordForm = document.getElementById('addPassword'),// форма добавления radio
        addTitleInput = document.getElementById('addTitle'),//инпут добавления заголовка
        addEmailInput = document.getElementById('addEmail'),//инпут добавления заголовка
        addRangeInput = document.getElementById('addRange'),//инпут добавления заголовка
        addTextAreaForm = document.getElementById('addTextArea'), // хэндлер добавления текст инпутов
        addSubmitForm = document.getElementById('addSubmit'), // хэндлер добавления текст инпутов
        addResetForm = document.getElementById('addReset'), // хэндлер добавления текст инпутов
        container = document.getElementById('main'),// область вывода формы. Главный контейнер
        headerForm = document.getElementById('formHeader'),
        bodyForm = document.getElementById('formBody'),
        footerForm = document.getElementById('formFooter'),
        finalResult = document.getElementById('finalResult');

    /* на каждую li накидываю обработчик открытия внутреннего блока*/
    allItems.forEach(function(item){
        item.addEventListener('click',openEditBlock)
    });
    addTitleForm.addEventListener('submit',addTitle);
    addCheckboxForm.addEventListener('submit',addCheckbox);
    addRadioForm.addEventListener('submit',addRadio);
    addTextForm.addEventListener('submit',addText);
    addDateForm.addEventListener('click',addDate);
    addColorForm.addEventListener('click',addColor);
    addPasswordForm.addEventListener('click',addPassword);
    addTextAreaForm.addEventListener('click',addTextArea);
    addEmailInput.addEventListener('click',addEmail);
    addRangeInput.addEventListener('click',addRange);
    addSubmitForm.addEventListener('click',addButton);
    addResetForm.addEventListener('click',addButton);



    /* Вспомогательные функции */

    function closeForm(){
        this.closest('li').classList.remove('active');
    }
    function clearInput(){
        let inputLabel = this.childNodes,
            childArray = Array.from(inputLabel),
            inputs = [];
        childArray.forEach(function(item){
            if(item.className === 'edit-part'){
                inputs.push(item)
            }
        });
        inputs.forEach(function(item){
            item.childNodes.forEach(function(item){
                if(item.type === 'text'){
                    item.value = "";
                }
            });
        });
    }
    function searchValue(){
        let inputLabel = this.childNodes,
            childArray = Array.from(inputLabel),
            inputValue = {},
            inputs = [];
        childArray.forEach(function(item){
           if(item.className === 'edit-part'){
               inputs.push(item)
           }
        });
        inputs.forEach(function(item){
            item.childNodes.forEach(function(item){
                if(item.name === 'value'){
                    inputValue.value = item.value;
                }else if(item.name === 'name'){
                    inputValue.name = item.value;
                }
            });

        });
        return inputValue;
    }

    /* функция открытия внутреннего блока. Общая!!*/
    function openEditBlock(){
        let allOpens = document.querySelectorAll('.show-edit-block'),
            allActive = document.querySelectorAll('.active');
        allOpens.forEach(function(item){
           item.classList.remove('show-edit-block');
        });
        allActive.forEach(function(item){
            item.classList.remove('active');
        });
        this.classList.add('active');
    }
    /* Функция добавления заголовка*/
    function addTitle(e){
        e.preventDefault();
        headerForm.setAttribute('class','edit-form__header');
        let titleValue = addTitleInput.value,
            formTitle = document.createElement('h2'),
            oldTitle = document.querySelector('.form__title');
        formTitle.setAttribute('class','form__title');
        formTitle.dataset.prop="title";
        /* если уже есть заголовки, чистим*/
        if(oldTitle){
            headerForm.removeChild(oldTitle);
        }
        if(titleValue){
            formTitle.innerHTML = titleValue;
            headerForm.appendChild(formTitle);
        }
        /* обнуляем значения инпута*/
        clearInput.call(this);
        /* закрывает редактор заголовка*/
        closeForm.call(this);
    }

    /* Функция добавления textArea*/
    function addTextArea(){
        let newTextArea = document.createElement('textarea');
        newTextArea.setAttribute('class','form__textarea');
        newTextArea.setAttribute('placeholder','Your message....');
        newTextArea.dataset.prop="message";
        newTextArea.dataset.value="message";
        bodyForm.appendChild(newTextArea);
    }

    /* Добавление checkbox */
    function addCheckbox(e){
        e.preventDefault();
        let newLabel = document.createElement('label'),
            newCheckbox = document.createElement('input'),
            valuesObj = searchValue.call(this);
        newCheckbox.setAttribute('type','checkbox');
        newCheckbox.dataset.prop="checkbox";
        newCheckbox.dataset.value=valuesObj.value;
        newLabel.innerHTML = valuesObj.value;
        newLabel.appendChild(newCheckbox);
        bodyForm.appendChild(newLabel);
        clearInput.call(this);
        closeForm.call(this);
    };
    /* Добавление radio */
    function addRadio(e){
        e.preventDefault();
        let newLabel = document.createElement('label'),
            newRadio = document.createElement('input'),
            valuesObj = searchValue.call(this);
        newRadio.setAttribute('type','radio');
        newRadio.setAttribute('name',valuesObj.name);
        newRadio.dataset.prop="radio";
        newRadio.dataset.value=valuesObj.value;
        newLabel.innerHTML = valuesObj.value;
        newLabel.appendChild(newRadio);
        bodyForm.appendChild(newLabel);
        clearInput.call(this);
        closeForm.call(this);
    }

    /* Добавление text инпутов */
    function addText(e){
        e.preventDefault();
        let newLabel = document.createElement('label'),
            newText = document.createElement('input'),
            valuesObj = searchValue.call(this);
        newText.setAttribute('type','text');
        newText.setAttribute('placeholder',valuesObj.value);
        newText.dataset.prop="text";
        newText.dataset.value=valuesObj.value;
        newLabel.innerHTML = valuesObj.value;
        newLabel.appendChild(newText);
        bodyForm.appendChild(newLabel);
        clearInput.call(this);
        closeForm.call(this);
    }

    /* Добавление даты */

    function addDate(){
        let dateContainer = document.createElement('div'),
            dateName = document.createElement('span'),
            dateValue = document.createElement('input');
        dateName.innerHTML = 'Select date: ';
        dateValue.setAttribute('type','date');
        dateValue.dataset.prop="date";
        dateValue.dataset.value="Selected date";
        dateContainer.appendChild(dateName);
        dateContainer.appendChild(dateValue);
        bodyForm.appendChild(dateContainer);
        clearInput.call(this);
        closeForm.call(this);
    }

    /* Выбор цвета */

    function addColor(){
        let colorContainer = document.createElement('div'),
            colorName = document.createElement('span'),
            colorValue = document.createElement('input');
        colorName.innerHTML = 'Select color: ';
        colorValue.setAttribute('type','color');
        colorValue.dataset.prop="color";
        colorValue.dataset.value="Color";
        colorContainer.appendChild(colorName);
        colorContainer.appendChild(colorValue);
        bodyForm.appendChild(colorContainer);
        clearInput.call(this);
        closeForm.call(this);
    }

    /* Добавление пароля */

    function addPassword(){
        let newLabel = document.createElement('span'),
            newContainer = document.createElement('div'),
            newText = document.createElement('input');
        newText.setAttribute('type','password');
        newText.setAttribute('placeholder','password');
        newText.dataset.prop="password";
        newText.dataset.value="Password";
        newLabel.innerHTML = 'Enter you password';
        newContainer.appendChild(newLabel);
        newContainer.appendChild(newText);
        bodyForm.appendChild(newContainer);
    }

    /* Добавление e-mail*/

    function addEmail(){
        let newLabel = document.createElement('span'),
            newContainer = document.createElement('div'),
            newText = document.createElement('input');
        newText.setAttribute('type','email');
        newText.setAttribute('placeholder','test@gmail.com.ua');
        newText.dataset.prop="email";
        newText.dataset.value="E-mail";
        newLabel.innerHTML = 'Enter you e-email';
        newContainer.appendChild(newLabel);
        newContainer.appendChild(newText);
        bodyForm.appendChild(newContainer);
    }

    /* Добавление Range */

    function addRange(){
        let newLabel = document.createElement('span'),
            showValue = document.createElement('span'),
            newContainer = document.createElement('div'),
            newText = document.createElement('input');
        newText.setAttribute('type','range');
        newText.setAttribute('min',0);
        newText.setAttribute('max',100);
        newText.dataset.prop="range";
        newText.dataset.value="Selected value";
        newLabel.innerHTML = 'Select value';
        showValue.setAttribute('class','show-value');
        newText.addEventListener('input',showCurrentValue);
        newContainer.appendChild(newLabel);
        newContainer.appendChild(newText);
        newContainer.appendChild(showValue);
        bodyForm.appendChild(newContainer);
    }
    function showCurrentValue(e){
        let currentValueRange = e.target.value,
            container = document.querySelector('.show-value');
        container.innerHTML = currentValueRange;
    };

    /* Добавление submit */

    function addButton(e){
        footerForm.setAttribute('class','edit-form__footer');
        let type = e.target.closest('li').dataset.type;
        let newButt = document.createElement('input');
        newButt.setAttribute('type',type);
        newButt.setAttribute('class','edit-form__button');
        newButt.dataset = "button";
        newButt.value = type;
        footerForm.appendChild(newButt);
    }
    container.addEventListener('submit',showResult)

    function showResult(e){
        e.preventDefault();
        let allFormElements = this.elements,
            allFormElementsArray = Array.from(allFormElements),
            resultInputs = {},
            resultChecks = {},
            resultRadio = {};
        allFormElementsArray.forEach(function(item){
            if(item.dataset.value){
                if(item.dataset.prop==='checkbox' && item.checked){
                    resultChecks[item.dataset.value] = item.value;
                }else if(item.dataset.prop==='radio' && item.checked){
                    resultInputs[item.name] = item.dataset.value;
                }else if(item.dataset.prop!=='checkbox' && item.dataset.prop!=='radio'){
                    resultInputs[item.dataset.value] = item.value;
                }

            }
        });



        if(resultInputs){
            let newTitle = document.createElement('p'),
                newList = document.createElement('ul');
            newTitle.innerHTML = 'You entered the following data:';
            for (var key in resultInputs) {
                let newItemList = document.createElement('li');
                newItemList.innerHTML = `${key}: ${resultInputs[key]}`;
                newList.appendChild(newItemList);
            }
            finalResult.appendChild(newTitle);
            finalResult.appendChild(newList);
        }
        if(resultChecks){
            let newTitle = document.createElement('p'),
                newList = document.createElement('ul');
            newTitle.innerHTML = 'Also, you sele`cted the following items :';
            for (var key in resultChecks) {
                let newItemList = document.createElement('li');
                newItemList.innerHTML = `${key}`;
                newList.appendChild(newItemList);
            }
            finalResult.appendChild(newTitle);
            finalResult.appendChild(newList);
        }

    }
});