


  /*
    Задание:
    1. При помощи методов изложеных ниже, переформатировать ITEA_COURSES в массив который содержит длину строк каждого из элементов.
    2. Самостоятельно изучить метод Array.sort. Отфильтровать массив ITEA_COURSES по алфавиту.
        + Бонусный бал. Вывести на страничку списком
    3. Реализация функции поиска по массиву ITEA_COURSES.
        + Бонусный бал. Вывести на страничку инпут и кнопку по которой будет срабатывать поиск.
  */



const ITEA_COURSES = ["Курс HTML & CSS", "JavaScript базовый курс", "JavaScript продвинутый курс", "JavaScript Professional", "Angular 2.4 (базовый)", "Angular 2.4 (продвинутый)", "React.js", "React Native", "Node.js", "Vue.js"];

/*  1. При помощи методов изложеных ниже, переформатировать ITEA_COURSES в массив который содержит длину строк каждого из элементов. */

const ITEA_COURSES_LENGTH = ITEA_COURSES.map((item) => item.length);


/* 2. Самостоятельно изучить метод Array.sort. Отфильтровать массив ITEA_COURSES по алфавиту.
        + Бонусный бал. Вывести на страничку списком*/

const ITEA_COURSES_SORT = ITEA_COURSES.sort();

let container = document.getElementById('result');

let list = document.createElement('ul');

ITEA_COURSES_SORT.forEach(function(item){
    let newListItem = document.createElement('li');
    newListItem.innerHTML = item;
    list.appendChild(newListItem);
});

container.appendChild(list);

/*  3. Реализация функции поиска по массиву ITEA_COURSES.
        + Бонусный бал. Вывести на страничку инпут и кнопку по которой будет срабатывать поиск.*/

let listSearch = document.createElement('ul');

let search = document.getElementById('search');

listSearch.classList.add('search-result');

const checkOtherResult = (arr) => {
    if(arr.length) {
        arr.forEach( (item) => listSearch.removeChild(item));
    }
}

const searchMatch = (e) => {
    e.preventDefault();
    let value = e.target.value.toLowerCase();




    let allSearchListItems = document.querySelectorAll('.search-result li');


    let resultSearch = ITEA_COURSES.filter((item) => item.toLowerCase().match(value));
    if(value){

        checkOtherResult(allSearchListItems);

        resultSearch.forEach((item) => {
            let newListItemSearch = document.createElement('li');
            newListItemSearch.innerHTML = item;
            listSearch.appendChild(newListItemSearch);
        });
        container.appendChild(listSearch);
    }else{
        checkOtherResult(allSearchListItems);
    }
}

search.addEventListener('input',searchMatch);


