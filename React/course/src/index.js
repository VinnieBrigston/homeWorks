import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

import App from './components/App';

import { Provider } from "react-redux";
import store from './store';

import { BrowserRouter} from 'react-router-dom';

const supportsHistory = 'pushState' in window.history;

ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter basename="/" forceRefrech={!supportsHistory}>
      <App/>
    </BrowserRouter>
  </Provider>,
  document.getElementById('root'));
