import { combineReducers } from 'redux';

const check_init = {
  showMainDisplay: false,
  showOrderList: false
}

const checkDisplay = (state = check_init, action) => {
  switch ( action.type ) {

    case 'SHOW_DISPLAY':
      return {
        ...state,
        showMainDisplay: !state.showMainDisplay
      };
    case 'SHOW_ORDER':
      return {
        ...state,
        showOrderList: !state.showOrderList
      }
  
    default:
      return state;
  }
}

const theater_init = {
  theater: [],
  toOrder : []
}
const theater = (state =theater_init, action) => {
  switch ( action.type ) {

    case 'FILL_PLACES':
      return {
        cinemaRoom : action.places
      }
    
    case 'CHECK_PLACE':
      let updateTheater = state.cinemaRoom.map((row) =>{
        return row.map((place) => 
          (place.id === action.id)
            ? {...place, checked: !place.checked}
            :  place
        )
      })
      return {
        cinemaRoom : updateTheater
      }
    case 'BOOK_PLACE':
      let theaterWithBookedPlaces = state.cinemaRoom.map((row) =>{
        return row.map((place) => 
          (place.checked)
            ? {...place, booked: !place.booked}
            :  place
        )
      })
      return {
        cinemaRoom : theaterWithBookedPlaces
      }
    default:
      return state;
  }
}
const reducer = combineReducers({
  theater,
  checkDisplay
});

export default reducer;
