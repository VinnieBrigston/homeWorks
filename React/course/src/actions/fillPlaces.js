export function fillPlaces (array) {
    return {
      type: 'FILL_PLACES',
      places : array
    }
}