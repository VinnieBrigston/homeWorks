import React, { Component } from 'react';
import { connect } from 'react-redux';
import {checkPlace} from '../actions/checkPlace';
import {showOrder} from '../actions/showOrder'
import imgDisplay from '../img/display.svg';
import {getPersonalId} from '../methods/getPersonalId';

class MainDisplay extends Component{
    
    bookingPlace = (personalId) => (event) => {
        let {checkPlace} = this.props;
        checkPlace(personalId);
    }
    makeOrderList = () =>{
        let {showOrder} = this.props;
        showOrder();
    }

    render = () => {
        let allPlaces = this.props.theater.cinemaRoom;
        return(
            <div className = "show-room">
                <img alt="img-display" src={imgDisplay} className="display-img"/>
                { 
                    allPlaces.length ?
                        allPlaces.map((row, key) => {
                            let rowIndex = allPlaces.indexOf(row);
                            return (
                                <div key={key} className="one-row">
                                    {
                                        row.map((place,key) =>{
                                            let personalId = getPersonalId(rowIndex,place.place);
                                            return (
                                                <div className="place-container" key={key}>
                                                    <ul className="place-tooltip">
                                                        <li> Place : {place.place}</li>
                                                        <li> Row : {place.row}</li>
                                                        <li> Price : {place.price}</li>
                                                    </ul>
                                                    <div id={personalId} className = {place.checked ? "one-place one-place_checked" : "one-place"} data-booked = {place.booked ? "booked" : "not-booked"} onClick = {this.bookingPlace(personalId)}></div>
                                                </div>
                                            )
                                        })
                                    }
                                </div>
                            )
                        })
                    :
                        <div>No places</div>
                }
                <button className="order-button" onClick={this.makeOrderList}>Order</button>
            </div>
        )
    }

}

const mapStateToProps = (state, ownProps) => {
    return {
        theater: state.theater
    };
};
export default connect(mapStateToProps,({checkPlace,showOrder}))(MainDisplay);