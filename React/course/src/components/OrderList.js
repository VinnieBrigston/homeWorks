import React, { Component } from 'react';
import { connect } from 'react-redux';
import {checkPlace} from '../actions/checkPlace';
import closeImg from '../img/delete.png';
import {showOrder} from '../actions/showOrder';
import {bookPlace} from '../actions/bookPlace';
import {getPersonalId} from '../methods/getPersonalId';

class OrderList extends Component{

    deleteItem = (personalId) => (event) => {
        this.props.checkPlace(personalId);
    }
    bookingPlace = () =>{
        let {bookPlace,showOrder} = this.props;
        bookPlace();
        showOrder();
    }
    render=()=>{
        let allPlaces = this.props.theater,
            totalPriceArray=[],
            {showOrder} = this.props,
            checkedPlaces = allPlaces.map((row) =>{
                return row.filter((place) =>
                    place.checked
                )
            }).filter((array) => array.length > 0);
        return(
            <div className={showOrder ? "order-wrap order-wrap_show" : "order-wrap"}>
                <ul className ="order-list">
                <img src={closeImg} className="close-order" onClick={this.props.showOrder} />
                {
                    checkedPlaces.length ?
                        allPlaces.map((row)=>{
                            return row.map((place,key)=>{
                                if(place.checked){
                                    let personalId = getPersonalId(place.row-1,place.place);
                                    return (
                                        <li key={key} className="order-item">
                                            <div>Place : {place.place}</div>
                                            <div>Row : {place.row}</div>
                                            <div>Price : {place.price}</div>
                                            <button className="order-delete" onClick={this.deleteItem(personalId)}>X</button>
                                        </li>
                                    )
                                }
                               
                               
                            })
                        })
                    :
                        <li> List is empty!</li>
                    }
                <li className="order-total">
                    <span>TOTAL:</span> <span>{
                        checkedPlaces.length ?
                            checkedPlaces.map((array) => {
                                let prices = array.map((place)=>
                                    place.price
                                )
                                return prices
                                
                            }).reduce((prev,cur) => prev.concat(cur)).reduce((a,b)=> a+b)
                            
                            
                        :
                            0
                    }</span>
                </li>
                <li className="accept-item"><button className="accept-order" onClick={this.bookingPlace}>ACCEPT</button></li>
                </ul>
            </div>
        )
    }
}

const mapStateToProps =(state, ownProps) => {
    return {
        theater: state.theater.cinemaRoom,
        showOrder : state.checkDisplay.showOrderList
    };
};
export default connect(mapStateToProps,({checkPlace,showOrder,bookPlace}))(OrderList);