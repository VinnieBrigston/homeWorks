import React, { Component } from 'react';
import { connect } from 'react-redux';
import {fillPlaces} from '../actions/fillPlaces';
import {showDisplay} from '../actions/showDisplay';
import {createPlaces} from '../methods/createPlaces';



class StartForm extends Component{
    state = {
        seats: 0,
        seatsInrow : 0
    };

    handlerChange = (e) => {
        let userValue = Number(e.target.value),
            inputName = e.target.name;
        switch (inputName) {
            case "seatsCount":
                this.setState({
                    seats : userValue
                });
                break;
            case "seatsInRow":
                this.setState({
                    seatsInrow : userValue
                });
                break;
            default:
                console.log( 'Search value not found' );
        }
    }
    onSubmit = (e) => {
        e.preventDefault();
        let {seats,seatsInrow} = this.state,
            {fillPlaces, showDisplay} = this.props,
            allSeats;

        allSeats = createPlaces(seats,seatsInrow),
        fillPlaces(allSeats);
        showDisplay();
        
        
    }
    render = () => {
        return(
            <form className = "start-form" onSubmit = {this.onSubmit}>
                <input
                    className ={"start-form__input start-form__input_text"}
                    name = {"seatsCount"}
                    type={"text"}
                    placeholder={"Seats number"}
                    onChange = {this.handlerChange}
                />
                <input
                    className ={"start-form__input start-form__input_text"}
                    name ={"seatsInRow"}
                    type={"text"}
                    placeholder={"Seats in row"}
                    onChange = {this.handlerChange}
                />
                <button
                    className ={"start-form__input start-form__input_submit"}
                    type={"submit"}
                >
                    accept
                </button>
            </form>
        )
    }
}
const mapStateToProps = (state, ownProps) => {
    return {
      theater: state.theater.cinemaRoom
    };
};
export default connect(mapStateToProps,({fillPlaces,showDisplay}))(StartForm)