import React, { Component } from 'react';
import './App.css';
import StartForm from './StartForm';
import MainDisplay from './MainDisplay';
import OrderList from './OrderList';
import { connect } from 'react-redux';

class App extends Component {
  render() {
   let {showDisplay,showOrder} = this.props;
    return(
      <div className="start">
        {
          showDisplay ? 
            <MainDisplay />
          :
            <StartForm />
        }
        {
          showOrder && <OrderList />
        }
        
      </div>
    )
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    showDisplay: state.checkDisplay.showMainDisplay,
    showOrder : state.checkDisplay.showOrderList
  };
};
export default connect(mapStateToProps,null)(App);

