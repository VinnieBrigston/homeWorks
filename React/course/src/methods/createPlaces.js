import {createPlace} from './createPlace';
export function createPlaces(numberSeats,numberSeatsInRow){
    let theater = new Array(),
        numberOfRows = Math.floor(numberSeats / numberSeatsInRow);
    for (let i=0;i<numberOfRows;i++) {
        theater[i]=new Array();
        for (let j=1;j<=numberSeatsInRow;j++) {
            theater[i][j]= createPlace(i,j);
        }
    }
    let remainder = numberSeats - (numberSeatsInRow * numberOfRows);
    if(remainder > 0){
        let lastRow = [];
        for (let t=1;t<=remainder;t++) {
            lastRow.push(createPlace(numberOfRows,t));
        }
        theater.push(lastRow);
    }
    return theater;
}
