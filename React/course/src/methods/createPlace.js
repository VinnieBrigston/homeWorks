export function createPlace(row,number){
    let realRow = row+1;
    let place = {
        checked: false,
        booked: false,
        place: number,
        price: function(){if(realRow > 3) { return 150}else if(realRow >5) {return 245}else {return 75}}(),
        row: realRow,
        id: Number(String(row)+String(number))
    }
    return place;
}