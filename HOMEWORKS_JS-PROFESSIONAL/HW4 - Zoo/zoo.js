/*

  Задание - используя классы и (или) прототипы создать программу, которая будет
  распределять животных по зоопарку.

  Zoo ={
    name: '',
    AnimalCount: 152,
    zones: {
      mammals: [],
      birds: [],
      fishes: [],
      reptile: [],
      others: []
    },
    addAnimal: function(animalObj){
      // Добавляет животное в зоопарк в нужную зону.
      // зона берется из класса который наследует Animal
      // если у животного нету свойства zone - то добавлять в others
    },
    removeAnimal: function('animalName'){
      // удаляет животное из зоопарка
      // поиск по имени
    },
    getAnimal: function(type, value){
      // выводит информацию о животном
      // поиск по имени или типу где type = name/type
      // а value значение выбраного типа поиска
    },
    countAnimals: function(){
      // функция считает кол-во всех животных во всех зонах
      // и выводит в консоль результат
    }
  }

  Есть родительский класс Animal у которого есть методы и свойства:
  Animal {
    name: 'Rex', // имя животного для поиска
    phrase: 'woof!',
    foodType: 'herbivore' | 'carnivore', // Травоядное или Плотоядное животное
    eatSomething: function(){ ... }
  }

  Дальше будут классы, которые расширяют класс Animal - это классы:
  - mammals
  - birds
  - fishes
  - pertile

  каждый из них имеет свои свойства и методы:
  в данном примере уникальными будут run/speed. У птиц будут методы fly & speed и т.д
  Mammals = {
    zone: mamal, // дублирует название зоны, ставиться по умолчанию
    type: 'wolf', // что за животное
    run: function(){
      console.log( wolf Rex run with speed 15 km/h );
    },
    speed: 15
  }

  Тестирование:
    new Zoo('name');
    var Rex = new Mammal('Rex', 'woof', 'herbivore', 15 );
    your_zooName.addAnimal(Rex);
      // Добавит в your_zooName.zones.mamals новое животное.
    var Dex = new Mammal('Dex', 'woof', 'herbivore', 11 );
    your_zooName.addAnimal(Dex);
      // Добавит в your_zooName.zones.mamals еще одно новое животное.

    your_zooName.get('name', 'Rex'); -> {name:"Rex", type: 'wolf' ...}
    your_zooName.get('type', 'wolf'); -> [{RexObj},{DexObj}];

    Программу можно расширить и сделать в виде мини игры с интерфейсом и сдать
    как курсовую работу!
    Идеи:
    - Добавить ранжирование на травоядных и хищников
    - добавив какую-то функцию которая иммитирует жизнь в зоопарке. Питание, движение, сон животных и т.д
    - Условия: Если хищник и травоядный попадает в одну зону, то хищник сьедает травоядное и оно удаляется из зоопарка.
    - Графическая оболочка под программу.
*/

class Animal{
    constructor(name,phrase,foodType){
        this.name = name;
        this.phrase = phrase;
        this.foodType = foodType;
    }
}

class Mammals extends Animal{
    constructor(name,phrase,foodType,type,speed){
        super(name,phrase,foodType);
        this.zone = 'mamal';
        this.speed = speed;
        this.type = type;
    };
    run(){
        console.log(`${this.type} ${this.name} run with speed ${this.speed} km/h `)
    }
}

class Birds extends Animal{
    constructor(name,phrase,foodType,type,speed){
        super(name,phrase,foodType);
        this.zone = 'birds';
        this.speed = speed;
        this.type = type;
    };
    fly(){
        console.log(`${this.type} ${this.name} fly with speed ${this.speed} km/h `)
    }
}

class Fishes extends Animal{
    constructor(name,phrase,foodType,type,speed){
        super(name,phrase,foodType);
        this.zone = 'fishes';
        this.speed = speed;
        this.type = type;
    };
    swim(){
        console.log(`${this.type} ${this.name} swim with speed ${this.speed} km/h `)
    }
}

class Rertile extends Animal{
    constructor(name,phrase,foodType,type,speed){
        super(name,phrase,foodType);
        this.zone = 'pertile';
        this.speed = speed;
        this.type = type;
    };
    swim(){
        console.log(`${this.type} ${this.name} swim with speed ${this.speed} km/h `)
    }
}
class Zoo {
    constructor(){
        this.zones ={
            mammals: [],
            birds: [],
            fishes: [],
            reptile: [],
            others: []
        };
    }
    addAnimal(animalObj){
        let zone = animalObj.zone;
        switch (zone) {
            case 'fishes':
                this.zones.fishes.push(animalObj);
                break;
            case 'mammals':
                this.zones.mammals.push(animalObj);
                break;
            case 'birds':
                this.zones.birds.push(animalObj);
                break;
            case 'reptile':
                this.zones.reptile.push(animalObj);
                break;
            default:
                this.zones.others.push(animalObj);
        }
    };
    removeAnimal(name){
        let allZones = this.zones;
        for (var key in allZones) {
            allZones[key].forEach(function(elem){
                if(elem.name === name){
                    let index = allZones[key].indexOf(elem);
                    allZones[key].splice(index,1);
                }
            })
        }
    };
    getAnimal(typeName,value){
        let allZones = this.zones,
            resultArr = [];
        for (var key in allZones) {
            allZones[key].forEach(function(elem){
                if(elem[typeName] === value){
                    resultArr.push(elem);
                }
            })
        }
        console.log(resultArr);
    };
    countAnimals(){
        let allZones = this.zones,
            count = 0;
        for (var key in allZones) {
            if(allZones[key].length){
                count+=allZones[key].length;
            }
        }
        console.log(`Всего животных: ${count}`)
    }

}
let Barry = new Fishes('Barry','Gool','carnivore','shark',40);
let Lenny = new Fishes('Lenny','Gool','carnivore','clowm',30);
let Leo = new Rertile('Leo','Gool','carnivore','croc',10);
Barry.swim();
let ZooKiev = new Zoo();

ZooKiev.addAnimal(Barry);
ZooKiev.addAnimal(Lenny);
ZooKiev.addAnimal(Leo);
console.log(ZooKiev);
ZooKiev.countAnimals();
