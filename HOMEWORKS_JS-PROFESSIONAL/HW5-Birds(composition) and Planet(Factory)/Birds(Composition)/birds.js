/*

  Задание:
    Написать конструктор используя паттерн композиции.
    Каждую функцию выделить в отдельный модуль и собрать.

    Тематика - птицы.
    Птицы могут:
      - Нестись
      - Летать
      - Плавать
      - Кушать
      - Охотиться
      - Петь
      - Переносить почту
      - Бегать

  Составить птиц (пару на выбор, не обязательно всех):
      Страус
      Голубь
      Курица
      Пингвин
      Чайка
      Ястреб
      Сова

 */
import Run from './modules/run.js';
import Eat from './modules/eat.js';
import Fly from './modules/fly.js';
import Swim from './modules/swim.js';
import Hunt from './modules/hunt.js';
import Sing from './modules/sing.js';
import Deliver from './modules/mail.js';


const Owl = (name) => {
    let state = {
        name
    };
    return Object.assign(
        Run(state),
        Eat(state),
        Fly(state),
        Hunt(state)
    );
};

const aliveOwl = Owl('Owl');
aliveOwl.run();
aliveOwl.eat();
aliveOwl.hunt();

const Pigeon = (name) => {
    let state = {
        name
    };
    return Object.assign(
        Run(state),
        Eat(state),
        Fly(state),
        Deliver(state),
        Sing(state)
    );
};

const alivePigeon = Pigeon('Pigeon');
alivePigeon.fly();
alivePigeon.deliver();
const Penguin = (name) => {
    let state = {
        name
    };
    return Object.assign(
        Run(state),
        Eat(state),
        Swim(state)
    );
};
const alivePenguin = Penguin('Penguin');
alivePenguin.swim();
alivePenguin.eat();
alivePenguin.run();


