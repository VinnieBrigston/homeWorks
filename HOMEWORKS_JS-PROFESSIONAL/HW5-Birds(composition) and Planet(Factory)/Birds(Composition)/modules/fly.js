const Fly = ( state ) => ({
    fly: () => {
        console.log(`${state.name} fly`);
    }
});

export default Fly;