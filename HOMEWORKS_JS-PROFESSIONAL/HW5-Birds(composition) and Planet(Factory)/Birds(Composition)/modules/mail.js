const Deliver = ( state ) => ({
    deliver: () => {
        console.log(`${state.name} delivers mail`);
    }
});

export default Deliver;