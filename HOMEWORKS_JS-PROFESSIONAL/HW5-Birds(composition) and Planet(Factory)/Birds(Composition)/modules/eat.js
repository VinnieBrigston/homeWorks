const Eat = ( state ) => ({
    eat: () => {
        console.log(`${state.name} eat`);
    }
});

export default Eat;