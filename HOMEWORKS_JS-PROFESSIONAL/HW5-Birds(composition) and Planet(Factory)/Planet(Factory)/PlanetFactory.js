const Planet = ( name ) => {
    // Приватнное свойсвто!
    let timeCycle = 24,
        population = randomPopulation(1000,10000);

    function randomNumberGen(min,max){
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
    function randomPopulation(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
    function growPopulation(){
        let randomNumber = randomNumberGen(1,10);
        let rate = randomNumber * 100;
        this.population = this.population + rate;
        console.log('За один цикл на планете ' + name  + ' прибавилось ' + population + ' людей')
    }
    function Cataclysm(){
        let randomNumber = randomNumberGen(1,10);
        let rate = randomNumber * 10000;
        this.population = this.population - rate;
        console.log('На планете ' + name  + ' умерло ' + population + ' людей')
    }



    var planetObj = Object.assign(
        {},
        {
            name: name,
            population: randomPopulation(1000,10000),
            rotatePlanet: () => {
                let randomNumber = Math.floor(Math.random() * (1000 - 1 + 1)) + 1;
                console.log(randomNumber);
                if ( (randomNumber % 2) === 0) {
                    growPopulation();
                 } else {
                    Cataclysm();
                }
                console.log( this.population )
            }
        }
    );

    return planetObj;
};

const Mars = Planet('Mars');
Mars.rotatePlanet();
Mars.rotatePlanet();
Mars.rotatePlanet();
Mars.rotatePlanet();
Mars.rotatePlanet();
Mars.rotatePlanet();
Mars.rotatePlanet();
Mars.rotatePlanet();
Mars.rotatePlanet();
Mars.rotatePlanet();
Mars.rotatePlanet();
