
const Booking = () => {
    /* Конструктор кинотеатра*/
    class Theater{
        constructor(places,placesInRow){
            this.places = this.createPlaces(places,placesInRow);
        }
        createPlace(number){
            let place = {
                checked: false,
                booked: false,
                place: number
            }
            return place;
        }
        createPlaces(numberSeats,numberSeatsInRow){
            let theater = new Array(),
                numberOfRows = Math.floor(numberSeats / numberSeatsInRow);
            for (let i=0;i<numberOfRows;i++) {
                theater[i]=new Array();
                for (let j=1;j<=numberSeatsInRow;j++) {
                    theater[i][j]= this.createPlace(j);
                }
            }
            let remainder = numberSeats - (numberSeatsInRow * numberOfRows);
            if(remainder > 0){
                let lastRow = [];
                for (let t=1;t<=remainder;t++) {
                    lastRow.push(this.createPlace(t));
                }
                theater.push(lastRow);
            }
            return theater;
        }
        selectPlace(rowNumber,placeNumber){
           this.places[rowNumber][placeNumber].checked === false ? this.places[rowNumber][placeNumber].checked = true : this.places[rowNumber][placeNumber].checked = false;
        }
        reserveSeats(){
            this.places.forEach((oneplace) => {
                if(oneplace.checked === false){
                    oneplace.booked = true;
                }
            })
        }
    }

    /* необходимые для работы переменные */
    let BlockBaster;
    let startForm = document.getElementById('startForm'),
        menu = document.getElementById('start'),
        cinemaRoom = document.getElementById('mainContainer'),
        orderMenu = document.getElementById('order'),
        orderList = document.getElementById('info'),
        totalSum = document.getElementById('sum'),
        submitOrderButton = document.getElementById('submitOrder'),
        submitButtonStart = document.getElementById('submitButtonStart');


    /* Функция выбора места*/
    let selectOneMorePlace = (rowNumber,seatNumber) => (event) =>{
        BlockBaster.selectPlace(rowNumber,seatNumber);
        event.target.classList.toggle('active-place');
    }

    /* Функция очистки списка заказа*/
    let clearOrderList = () => {
        let allItemsOfOrderList = orderList.querySelectorAll('li');
        if(allItemsOfOrderList.length > 0){
            allItemsOfOrderList.forEach(itemOfOrderList => orderList.removeChild(itemOfOrderList));
        }
    }

    /* Фукция вывода общей суммы заказа*/
    let showTotalAmount = (arrayOfSelectedForOrderPlaces) => {
        let allPrices = arrayOfSelectedForOrderPlaces.map((selectedForOrderPlace) => selectedForOrderPlace.price)
        let totalAmount = allPrices.reduce((a,b) => a+b);
        totalSum.innerHTML = `Total: ${totalAmount}`;
    }

    /* Функция формирования и обновления списка заказа*/
    let renderOrderList = () =>{
        clearOrderList();
        let ticketsForOrder = [];
        let placesForOrder = BlockBaster.places.forEach((row) => {
            row.forEach((place) => {
                if(place.checked === true){
                    place.row = BlockBaster.places.indexOf(row)+1;
                    if(place.row > 2 && place.row < 5){
                        place.price = 270;
                    }else if(place.row >= 5){
                        place.price = 320
                    }else {
                        place.price = 150;
                    }
                    ticketsForOrder.push(place)
                }
            })
        });
        ticketsForOrder.forEach((checkedPlace) => {
            let orderListItem = document.createElement('li');
            orderListItem.classList.add('order-point');
            orderListItem.innerHTML = `<p class="info-text"><span class="bold">${checkedPlace.place}</span><span>Place</span></p>
                                <p class="info-text"><span class="bold">${checkedPlace.row}</span><span>Row</span></p>
                                <p class="info-text"><span class="bold">${checkedPlace.price}</span><span>Price</span></p>
                                `;
            orderList.appendChild(orderListItem);
        })
        showTotalAmount(ticketsForOrder);
    }
    
    /* Фунция заполнения зала местами */
    let fillTheHallInPlaces = (arrayOfSeats) => {
        arrayOfSeats.forEach((row) => {
            let rowIndex = arrayOfSeats.indexOf(row),
                rowInTheater = document.createElement('div');
            rowInTheater.classList.add('row');
            row.forEach((seat) => {
                let seatIndex = row.indexOf(seat),
                    onePlace = document.createElement('div');
                onePlace.className = "one-place";
                onePlace.innerHTML = seat.place;
                onePlace.addEventListener('click',selectOneMorePlace(rowIndex,seatIndex));
                onePlace.addEventListener('click',renderOrderList);
                rowInTheater.appendChild(onePlace);
            })
            cinemaRoom.appendChild(rowInTheater);
        })
    }

    /* Функция подтверждения заказа */
    let submitOrder = () => {
        let allSelectedPlaces = document.querySelectorAll('.active-place');
        if(allSelectedPlaces.length > 0){
            allSelectedPlaces.forEach((selectPlace) => {
                selectPlace.classList.add('booked-place');
            })
        }
        BlockBaster.reserveSeats();
    }

    /* Со старта делаем кнопку сабмита формы неактивной*/
    submitButtonStart.setAttribute('disabled',true);
    submitButtonStart.classList.add('disable');


   /* Валидация стартовой формы*/
    let validationForm = () =>{
        let numberOfSeats = Number(document.getElementById('places').value),
            seatsInRow = Number(document.getElementById('placesInRow').value);
        if(numberOfSeats > 0 && seatsInRow > 0){
            submitButtonStart.removeAttribute('disabled');
            submitButtonStart.classList.remove('disable');
        }
    };

    /* Навешываем на сабмит формы введения кол-ва мест функцию */
    startForm.addEventListener('submit',(event) =>{
        event.preventDefault();
        let numberOfSeats = Number(document.getElementById('places').value),
            seatsInRow = Number(document.getElementById('placesInRow').value);
        BlockBaster = new Theater(numberOfSeats,seatsInRow);
        fillTheHallInPlaces(BlockBaster.places);
        menu.classList.add('hide');
        cinemaRoom.classList.add('show');
        cinemaRoom.classList.add('move');
        orderMenu.classList.add('open');
    })
    document.getElementById('places').addEventListener('input',validationForm);
    document.getElementById('placesInRow').addEventListener('input',validationForm);
    /* Вешаем на клик по кнопке заказа функцию*/
    submitOrderButton.addEventListener('click',submitOrder);
   
   
  
}; 
  
export default Booking;
