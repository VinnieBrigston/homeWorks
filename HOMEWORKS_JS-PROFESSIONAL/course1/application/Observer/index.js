import Demo1 from './demo1';
import Demo2 from './demo2';

const ObserverDemo = () => {

  // Abstract Demo
  // Demo1();

  // Functional Demo:
  Demo2();





}; //observer Demo

export default ObserverDemo;
