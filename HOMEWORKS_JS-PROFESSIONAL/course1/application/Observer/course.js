
const BookingTickets = () => {

    /* Берем значения из стартового pop-up для построения кино зала */
    let startForm = document.getElementById('startForm'),
        menu = document.getElementById('start'),
        container = document.getElementById('mainContainer'),
        order = document.getElementById('order'),
        infoBlock = document.getElementById('info'),
        totalSum = document.getElementById('sum'),
        submitOrder = document.getElementById('submitOrder'),
        orderArr = [],
        sum,
        allSum,
        result = [];


    /* Функция бронирования выбранных мест*/

    /* через indexOf попробовать*/
    let resumeOrder = (obj) => (event) => {
        console.log(result);
        console.log(obj);
        let allPlaces = document.querySelectorAll('.one-place'),
            idElems = obj.map((element) => element.id);
        obj.forEach((item) => {item.status='booked'});
        allPlaces.forEach((el) => {
            idElems.forEach((id) => {
                /*в отдельную функцию */
                if(Number(el.id) === id) el.classList.add('booked-place');
            })
        })
        
    }

    /* Функция выбора места */    
    let selectPlace = (elem) => (event) => {
        let orderPoint = document.createElement('div');
        orderArr.push(elem);
        orderPoint.classList.add('order-point');
        container.classList.add('move');
        order.classList.add('open');
        event.target.classList.add('active-place');
        orderPoint.innerHTML = `<p class="info-text"><span class="bold">${elem.place}</span><span>Place</span></p>
                                <p class="info-text"><span class="bold">${elem.row}</span><span>Row</span></p>
                                <p class="info-text"><span class="bold">${elem.price}</span><span>Price</span></p>
                                `;
        orderPoint.dataset.id = elem.id;
        infoBlock.appendChild(orderPoint);
        sum = orderArr.map((element) => element.price);
        /* все выбранные объекты закидываются в массив и перебором редьюсом подсчитываем общую сумму*/
        allSum = sum.reduce((a,b) => a+b);
        totalSum.innerHTML = `Total: ${allSum}`;
    }
    submitOrder.addEventListener('click',resumeOrder(orderArr))

    /* Вешаем на сабмит формы. Функция построения кинозала*/
    startForm.addEventListener('submit',(event) =>{
        let places = document.getElementById('places').value,
            totals = [];
        event.preventDefault();
        console.log(`кол-во мест - ${Number(places)}`);
        menu.classList.add('hide');
        /* На каждое место создаем объект */

        /* переписать под map */
        for(let i=1; i <= Number(places);i++){
            let placeObj = {
                place: i,
                price : 350,
                status: 'free',
                id: i
            } /* условием закидываем в объект с место соответсвующий номер ряда*/
            if(i > 10 && i <= 20){
                placeObj.row = 2;
            }else if( i > 20){
                placeObj.row = Number(String(i).slice(0,1)) + 1;
            }else{
                placeObj.row = 1;
            }
            totals.push(placeObj);
        }
        /* В зависимости от ряда добавляем каждому месту цену */
        result = totals.map((elem) => {
            if(elem.row < 4){
                elem.price = 150;
                return elem;
            }else if(elem.row >= 4 && elem.row < 7){
                elem.price = 270;
                return elem;
            }else{
                elem.price = 420;
                return elem;
            }
        })
        /* для каждого создаем div, рендерим в контейнер и навешаем функцию по клику*/
        result.forEach((elem) => {
            let onePlace = document.createElement('div'),
                tooltip = document.createElement('div');
            onePlace.className = "one-place";
            onePlace.setAttribute('id',elem.id);
            onePlace.innerHTML = elem.place;
            onePlace.addEventListener('click',selectPlace(elem));
            container.appendChild(onePlace);
            
        })
        container.classList.add('show');
        return result;
    })

    
   
  
  
  }; 
  
export default BookingTickets;
