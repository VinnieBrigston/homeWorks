/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./application/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./application/Observer/CustomEvents.js":
/*!**********************************************!*\
  !*** ./application/Observer/CustomEvents.js ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n\r\nconst CustomEvents = () => {\r\n\r\n  /*\r\n    JS позволяет нам создавать собственные события.\r\n    Синтаксис:\r\n      Обязательное только имя события:\r\n      new Event('eventName', {bubbles: true, cancelable: false});\r\n\r\n      Кастом ивент еще имеет доп. поле \"detail\", через которое можно\r\n      передать данные:\r\n      new CustomEvent(\"userLogin\", { detail: {...} });\r\n\r\n  */\r\n  let itea_event = new Event('build');\r\n  let itea_customEvent = new CustomEvent(\"userLogin\", {\r\n    detail: {\r\n      username: \"davidwalsh\"\r\n    }\r\n  });\r\n\r\n  // Абстрактный пример:\r\n  let myElement = document.createElement('button');\r\n      myElement.innerText = 'Custom Event!';\r\n      myElement.addEventListener(\"userLogin\", function(e) {\r\n        console.info(\"Event is: \", e);\r\n        console.info(\"Custom data is: \", e.detail);\r\n        myElement.style.backgroundColor = 'blue';\r\n      });\r\n\r\n      myElement.addEventListener('click', function(){\r\n        myElement.dispatchEvent(itea_customEvent);\r\n      });\r\n      document.body.appendChild(myElement);\r\n\r\n  // Реальный пример:\r\n\r\n  let message__container = document.getElementById('message__container');\r\n  let messageAuthor = document.getElementById('messageAuthor');\r\n  let messageText = document.getElementById('messageText');\r\n  let messageSendBtn = document.getElementById('messageSendBtn');\r\n\r\n      messageSendBtn.addEventListener('click', function(){\r\n        let author = messageAuthor.value;\r\n        let text = messageText.value;\r\n        let MessageSendEvent = new CustomEvent(\"messageSend\", {\r\n          detail: {\r\n            author: author,\r\n            message: text\r\n          }\r\n        });\r\n        message__container.dispatchEvent(MessageSendEvent);\r\n      });\r\n\r\n      message__container.addEventListener('messageSend', function(event){\r\n        let { author, message } = event.detail;\r\n        let messageNode = document.createElement('div');\r\n            messageNode.innerHTML = `<div class=\"message\"><b>${author}:</b> ${message}</div>`;\r\n        message__container.appendChild(messageNode);\r\n      });\r\n\r\n\r\n\r\n}; // custom events end!\r\n\r\n/* harmony default export */ __webpack_exports__[\"default\"] = (CustomEvents);\r\n\n\n//# sourceURL=webpack:///./application/Observer/CustomEvents.js?");

/***/ }),

/***/ "./application/Observer/Observer.js":
/*!******************************************!*\
  !*** ./application/Observer/Observer.js ***!
  \******************************************/
/*! exports provided: Observable, Observer */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Observable\", function() { return Observable; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Observer\", function() { return Observer; });\nfunction Observable(){\r\n  // Создаем список подписаных обьектов\r\n  var observers = [];\r\n  // Оповещение всех подписчиков о сообщении\r\n  this.sendMessage = function( msg ){\r\n      observers.map( ( obs ) => {\r\n        obs.notify(msg);\r\n      });\r\n  };\r\n  // Добавим наблюдателя\r\n  this.addObserver = function( observer ){\r\n    observers.push( observer );\r\n  };\r\n}\r\n\r\n// Сам наблюдатель:\r\nfunction Observer( behavior ){\r\n  // Делаем функцию, что бы через callback можно\r\n  // было использовать различные функции внутри\r\n  this.notify = function( msg ){\r\n    behavior( msg );\r\n  };\r\n}\r\n\r\n\r\n\n\n//# sourceURL=webpack:///./application/Observer/Observer.js?");

/***/ }),

/***/ "./application/Observer/demo1.js":
/*!***************************************!*\
  !*** ./application/Observer/demo1.js ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _Observer__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Observer */ \"./application/Observer/Observer.js\");\n\r\n\r\nconst Demo1 = () => {\r\n\r\n  let observable = new _Observer__WEBPACK_IMPORTED_MODULE_0__[\"Observable\"]();\r\n  let obs1 = new _Observer__WEBPACK_IMPORTED_MODULE_0__[\"Observer\"]( (msg) => console.log(msg));\r\n  let obs2 = new _Observer__WEBPACK_IMPORTED_MODULE_0__[\"Observer\"]( (msg) => console.warn(msg));\r\n  let obs3 = new _Observer__WEBPACK_IMPORTED_MODULE_0__[\"Observer\"]( (msg) => console.error(msg));\r\n\r\n      observable.addObserver( obs1 );\r\n      observable.addObserver( obs2 );\r\n      observable.addObserver( obs3 );\r\n\r\n  console.log( observable );\r\n\r\n  //  Проверим абстрактно как оно работает:\r\n  setTimeout(\r\n    ()=>{\r\n      // оправим сообщение, с текущей датой:\r\n      observable.sendMessage('Now is' + new Date());\r\n    }, 2000\r\n  );\r\n\r\n\r\n};\r\n\r\n/* harmony default export */ __webpack_exports__[\"default\"] = (Demo1);\r\n\n\n//# sourceURL=webpack:///./application/Observer/demo1.js?");

/***/ }),

/***/ "./application/Observer/demo2.js":
/*!***************************************!*\
  !*** ./application/Observer/demo2.js ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _Observer__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Observer */ \"./application/Observer/Observer.js\");\n\r\n\r\nconst Demo2 = () => {\r\n\r\n  /*\r\n    Рассмотрим на примере интернет магазина:\r\n  */\r\n\r\n  let Products = [\r\n    {\r\n      id: 1,\r\n      name: 'Samsung Galaxy S8 ',\r\n      price: 21999,\r\n      imageLink: 'https://i1.rozetka.ua/goods/1894533/samsung_galaxy_s8_64gb_black_images_1894533385.jpg'\r\n    },\r\n    {\r\n      id: 2,\r\n      name: 'Apple AirPort Capsule',\r\n      price: 10700,\r\n      imageLink: 'https://i1.rozetka.ua/goods/3330569/apple_a1470_me177_images_3330569615.jpg'\r\n    },\r\n    {\r\n      id: 3,\r\n      name: 'Apple iPhone X',\r\n      price: 35999,\r\n      imageLink: 'https://i1.rozetka.ua/goods/2433231/apple_iphone_x_64gb_silver_images_2433231297.jpg'\r\n    },\r\n    {\r\n      id: 4,\r\n      name: 'LG G6 Black ',\r\n      price: 15999,\r\n      imageLink: 'https://i1.rozetka.ua/goods/1892329/copy_lg_lgh845_acistn_58d8fc4a87d51_images_1892329834.jpg'\r\n    }\r\n  ];\r\n\r\n  // Создадим наблюдателя:\r\n  let observable = new _Observer__WEBPACK_IMPORTED_MODULE_0__[\"Observable\"]();\r\n  // Трех обсерверов:\r\n  let basketObs = new _Observer__WEBPACK_IMPORTED_MODULE_0__[\"Observer\"]( function(id){\r\n    let filtredToBasket = Products.filter( item => Number(item.id) === Number(id) );\r\n        // console.log();\r\n        Cart.push( filtredToBasket[0] );\r\n        renderBasket();\r\n  });\r\n\r\n  let serverObs = new _Observer__WEBPACK_IMPORTED_MODULE_0__[\"Observer\"]( (id) => {\r\n      let filtredToBasket = Products.filter( item => Number(item.id) === Number(id) );\r\n      let msg = `Товар ${filtredToBasket[0].name} добавлен в корзину`;\r\n      console.log( msg );\r\n  });\r\n\r\n  let iconObs = new _Observer__WEBPACK_IMPORTED_MODULE_0__[\"Observer\"]( (id) => {\r\n      let filtredToBasket = Products.filter( item => Number(item.id) === Number(id) );\r\n\r\n      let products__cart = document.getElementById('products__cart');\r\n          products__cart.innerText = Cart.length;\r\n  });\r\n\r\n  observable.addObserver( basketObs );\r\n  observable.addObserver( serverObs );\r\n  observable.addObserver( iconObs );\r\n\r\n  // Render Data - - - - - - - - - - - -  \r\n  let Cart = [];\r\n  let products__row = document.getElementById('products__row');\r\n\r\n  function renderBasket(){\r\n    let cartElem = document.getElementById('cart');\r\n    let message;\r\n        if( Cart.length === 0 ){\r\n          message = 'У вас в корзине пусто';\r\n        } else {\r\n          let Sum = Cart.reduce( (prev, current) => {\r\n            return prev += Number(current.price);\r\n          }, 0);\r\n          message = `У вас в корзине ${Cart.length} товаров, на сумму: ${Sum} грн.`;\r\n        }\r\n        cartElem.innerHTML = `<h2>${message}</h2><ol></ol>`;\r\n\r\n        let ol = cartElem.querySelector('ol');\r\n        Cart.map( item => {\r\n          let li = document.createElement('li');\r\n              li.innerText = `${item.name} (${item.price} грн.)`;\r\n              ol.appendChild(li);\r\n        });\r\n  }\r\n\r\n\r\n  Products.map( item => {\r\n    let product = document.createElement('div');\r\n        product.className = \"product\";\r\n        product.innerHTML =\r\n        `<div class=\"product__image\">\r\n            <img src=\"${item.imageLink}\"/>\r\n          </div>\r\n          <div class=\"product__name\">${item.name}</div>\r\n          <div class=\"product__price\">${item.price} грн.</div>\r\n          <div class=\"product__action\">\r\n            <button class=\"product__buy\" data-id=${item.id}> Купить </button>\r\n          </div>`;\r\n        let buyButton = product.querySelector('.product__buy');\r\n            buyButton.addEventListener('click', (e) => {\r\n              let id = e.target.dataset.id;\r\n              observable.sendMessage(id);\r\n            });\r\n        products__row.appendChild(product);\r\n  });\r\n\r\n  renderBasket();\r\n};\r\n\r\n/* harmony default export */ __webpack_exports__[\"default\"] = (Demo2);\r\n\n\n//# sourceURL=webpack:///./application/Observer/demo2.js?");

/***/ }),

/***/ "./application/Observer/index.js":
/*!***************************************!*\
  !*** ./application/Observer/index.js ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _demo1__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./demo1 */ \"./application/Observer/demo1.js\");\n/* harmony import */ var _demo2__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./demo2 */ \"./application/Observer/demo2.js\");\n\r\n\r\n\r\nconst ObserverDemo = () => {\r\n\r\n  // Abstract Demo\r\n  // Demo1();\r\n\r\n  // Functional Demo:\r\n  Object(_demo2__WEBPACK_IMPORTED_MODULE_1__[\"default\"])();\r\n\r\n\r\n\r\n\r\n\r\n}; //observer Demo\r\n\r\n/* harmony default export */ __webpack_exports__[\"default\"] = (ObserverDemo);\r\n\n\n//# sourceURL=webpack:///./application/Observer/index.js?");

/***/ }),

/***/ "./application/index.js":
/*!******************************!*\
  !*** ./application/index.js ***!
  \******************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _Observer__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Observer */ \"./application/Observer/index.js\");\n/* harmony import */ var _Observer_CustomEvents__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Observer/CustomEvents */ \"./application/Observer/CustomEvents.js\");\n/* harmony import */ var _theater__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./theater */ \"./application/theater.js\");\n// Точка входа в наше приложение\r\n\r\n\r\n// import BookingTickets from './Observer/course';\r\n\r\n\r\nObject(_theater__WEBPACK_IMPORTED_MODULE_2__[\"default\"])();\n\n//# sourceURL=webpack:///./application/index.js?");

/***/ }),

/***/ "./application/theater.js":
/*!********************************!*\
  !*** ./application/theater.js ***!
  \********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n\r\nconst Booking = () => {\r\n    \r\n    class Theater{\r\n        constructor(places,placesInRow){\r\n            this.places = this.createPlaces(places,placesInRow);\r\n        }\r\n        createPlace(number){\r\n            let place = {\r\n                checked: false,\r\n                booked: false,\r\n                place: number\r\n            }\r\n            return place;\r\n        }\r\n        createPlaces(numberSeats,numberSeatsInRow){\r\n            let theater = new Array(),\r\n                numberOfRows = Math.floor(numberSeats / numberSeatsInRow);\r\n            for (let i=0;i<numberOfRows;i++) {\r\n                theater[i]=new Array();\r\n                for (let j=1;j<=numberSeatsInRow;j++) {\r\n                    theater[i][j]= this.createPlace(j);\r\n                }\r\n            }\r\n            let remainder = numberSeats - (numberSeatsInRow * numberOfRows);\r\n            if(remainder > 0){\r\n                let lastRow = [];\r\n                for (let t=1;t<=remainder;t++) {\r\n                    lastRow.push(this.createPlace(t));\r\n                }\r\n                theater.push(lastRow);\r\n            }\r\n            return theater;\r\n        }\r\n        selectPlace(rowNumber,placeNumber){\r\n           this.places[rowNumber][placeNumber].checked === false ? this.places[rowNumber][placeNumber].checked = true : this.places[rowNumber][placeNumber].checked = false;\r\n        }\r\n        reserveSeats(){\r\n            this.places.forEach((oneplace) => {\r\n                if(oneplace.checked === false){\r\n                    oneplace.booked = true;\r\n                }\r\n            })\r\n        }\r\n    }\r\n\r\n    /* необходимые для работы переменные */\r\n    let BlockBaster;\r\n    let startForm = document.getElementById('startForm'),\r\n        menu = document.getElementById('start'),\r\n        cinemaRoom = document.getElementById('mainContainer'),\r\n        orderMenu = document.getElementById('order'),\r\n        orderList = document.getElementById('info'),\r\n        totalSum = document.getElementById('sum'),\r\n        submitOrderButton = document.getElementById('submitOrder'),\r\n        submitButtonStart = document.getElementById('submitButtonStart');\r\n\r\n\r\n    /* Функция выбора места*/\r\n    let selectOneMorePlace = (rowNumber,seatNumber) => (event) =>{\r\n        BlockBaster.selectPlace(rowNumber,seatNumber);\r\n        event.target.classList.toggle('active-place');\r\n    }\r\n\r\n    /* Функция очистки списка заказа*/\r\n    let clearOrderList = () => {\r\n        let allItemsOfOrderList = orderList.querySelectorAll('li');\r\n        if(allItemsOfOrderList.length > 0){\r\n            allItemsOfOrderList.forEach(itemOfOrderList => orderList.removeChild(itemOfOrderList));\r\n        }\r\n    }\r\n\r\n    /* Фукция вывода общей суммы заказа*/\r\n    let showTotalAmount = (arrayOfSelectedForOrderPlaces) => {\r\n        let allPrices = arrayOfSelectedForOrderPlaces.map((selectedForOrderPlace) => selectedForOrderPlace.price)\r\n        let totalAmount = allPrices.reduce((a,b) => a+b);\r\n        totalSum.innerHTML = `Total: ${totalAmount}`;\r\n    }\r\n\r\n    /* Функция формирования и обновления списка заказа*/\r\n    let renderOrderList = () =>{\r\n        clearOrderList();\r\n        let ticketsForOrder = [];\r\n        let placesForOrder = BlockBaster.places.forEach((row) => {\r\n            row.forEach((place) => {\r\n                if(place.checked === true){\r\n                    place.row = BlockBaster.places.indexOf(row)+1;\r\n                    if(place.row > 2 && place.row < 5){\r\n                        place.price = 270;\r\n                    }else if(place.row >= 5){\r\n                        place.price = 320\r\n                    }else {\r\n                        place.price = 150;\r\n                    }\r\n                    ticketsForOrder.push(place)\r\n                }\r\n            })\r\n        });\r\n        ticketsForOrder.forEach((checkedPlace) => {\r\n            let orderListItem = document.createElement('li');\r\n            orderListItem.classList.add('order-point');\r\n            orderListItem.innerHTML = `<p class=\"info-text\"><span class=\"bold\">${checkedPlace.place}</span><span>Place</span></p>\r\n                                <p class=\"info-text\"><span class=\"bold\">${checkedPlace.row}</span><span>Row</span></p>\r\n                                <p class=\"info-text\"><span class=\"bold\">${checkedPlace.price}</span><span>Price</span></p>\r\n                                `;\r\n            orderList.appendChild(orderListItem);\r\n        })\r\n        showTotalAmount(ticketsForOrder);\r\n    }\r\n    \r\n    /* Фунция заполнения зала местами */\r\n    let fillTheHallInPlaces = (arrayOfSeats) => {\r\n        arrayOfSeats.forEach((row) => {\r\n            let rowIndex = arrayOfSeats.indexOf(row),\r\n                rowInTheater = document.createElement('div');\r\n            rowInTheater.classList.add('row');\r\n            row.forEach((seat) => {\r\n                let seatIndex = row.indexOf(seat),\r\n                    onePlace = document.createElement('div');\r\n                onePlace.className = \"one-place\";\r\n                onePlace.innerHTML = seat.place;\r\n                onePlace.addEventListener('click',selectOneMorePlace(rowIndex,seatIndex));\r\n                onePlace.addEventListener('click',renderOrderList);\r\n                rowInTheater.appendChild(onePlace);\r\n            })\r\n            cinemaRoom.appendChild(rowInTheater);\r\n        })\r\n    }\r\n\r\n    /* Функция подтверждения заказа */\r\n    let submitOrder = () => {\r\n        let allSelectedPlaces = document.querySelectorAll('.active-place');\r\n        if(allSelectedPlaces.length > 0){\r\n            allSelectedPlaces.forEach((selectPlace) => {\r\n                selectPlace.classList.add('booked-place');\r\n            })\r\n        }\r\n        BlockBaster.reserveSeats();\r\n    }\r\n    submitButtonStart.setAttribute('disabled',true);\r\n    submitButtonStart.classList.add('disable');\r\n\r\n\r\n   \r\n    let validationForm = () =>{\r\n        let numberOfSeats = Number(document.getElementById('places').value),\r\n            seatsInRow = Number(document.getElementById('placesInRow').value);\r\n        if(numberOfSeats > 0 && seatsInRow > 0){\r\n            submitButtonStart.removeAttribute('disabled');\r\n            submitButtonStart.classList.remove('disable');\r\n        }\r\n    };\r\n\r\n    /* Навешываем на сабмит формы введения кол-ва мест функцию */\r\n    startForm.addEventListener('submit',(event) =>{\r\n        event.preventDefault();\r\n        let numberOfSeats = Number(document.getElementById('places').value),\r\n            seatsInRow = Number(document.getElementById('placesInRow').value);\r\n        BlockBaster = new Theater(numberOfSeats,seatsInRow);\r\n        fillTheHallInPlaces(BlockBaster.places);\r\n        menu.classList.add('hide');\r\n        cinemaRoom.classList.add('show');\r\n        cinemaRoom.classList.add('move');\r\n        orderMenu.classList.add('open');\r\n    })\r\n    document.getElementById('places').addEventListener('input',validationForm);\r\n    document.getElementById('placesInRow').addEventListener('input',validationForm);\r\n    /* Вешаем на клик по кнопке заказа функцию*/\r\n    submitOrderButton.addEventListener('click',submitOrder);\r\n   \r\n   \r\n  \r\n}; \r\n  \r\n/* harmony default export */ __webpack_exports__[\"default\"] = (Booking);\r\n\n\n//# sourceURL=webpack:///./application/theater.js?");

/***/ })

/******/ });