/*
  Задание:

    Написать синглтон, который будет создавать обьект government

    Данные:
    {
        laws: [],
        budget: 1000000
        citizensSatisfactions: 0,
    }

    У этого обьекта будут методы:
      .добавитьЗакон({id, name, description})
        -> добавляет закон в laws и понижает удовлетворенность граждан на -10

      .читатькКонституцию -> Вывести все законы на экран
      .читатьЗакон(ид)

      .показатьУровеньДовольства()
      .показатьБюджет()
            
      .провестиПраздник -> отнимает от бюджета 50000, повышает удовлетворенность граждан на +5
*/

const _data = {
    laws: [],
    budget: 1000000,
    citizensSatisfactions: 0,
};

const Constitution = {
    add: item => {_data.laws.push(item); _data.citizensSatisfactions -=10;},
    read: id => _data.laws.find( d=> d.id === id ),
    readAll: () => _data.laws.forEach(function(item){console.log(item)}),
    showSatisfaction: () => console.log(`Уровень удовлетворения населения на текущий момент равен: ${_data.citizensSatisfactions}`),
    showBudget: () => console.log(`Размер бюджета страны эквивалентен : ${_data.budget}`),
    spendHoliday: () => {
        _data.budget -= 50000;
        _data.citizensSatisfactions +=5;
    }
};
Object.freeze(Constitution);

export default Constitution;
