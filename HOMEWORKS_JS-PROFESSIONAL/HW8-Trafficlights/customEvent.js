/*
  Задание:  Открыть файл task1.html в папке паблик и настроить светофоры в
            соответсвии с правилавми ниже:

  1. Написать кастомные события которые будут менять статус светофора:
  - start: включает зеленый свет
  - stop: включает красный свет
  - night: включает желтый свет, который моргает с интервалом в 1с.
  И зарегистрировать каждое через addEventListener на каждом из светофоров.

  2.  Сразу после загрузки на каждом светофоре вызывать событие night, для того,
      чтобы включить режим "нерегулируемого перекрестка" (моргающий желтый).

  3.  По клику на любой из светофоров нунжо включать на нем поочередно красный (не первый клик)
      или зеленый (на второй клик) цвет соотвественно.
      Действие нужно выполнить только диспатча событие зарегистрированое в пункте 1.

  4.  + Бонус: На кнопку "Start Night" повесить сброс всех светофоров с их текущего
      статуса, на мигающий желтые.
      Двойной, тройной и более клики на кнопку не должны вызывать повторную
      инициализацию инвервала.

*/

let Night = new CustomEvent("night");
let Start = new CustomEvent("start");
let Stop = new CustomEvent("stop");
let nightMode = function(){event.target.classList.toggle('yellow')};
const TrafficEvents = () => {
    let allTrafficlights = document.querySelectorAll('.trafficLight');
    let buttonDo = document.getElementById('Do');
    let container = document.getElementById('app');
    window.addEventListener('load', function(){

        
        allTrafficlights.forEach(function(elem){
            elem.addEventListener('night',nightMode);
            elem.dataset.intId = setInterval(function(){
                elem.dispatchEvent(Night);
            },1000)
            
            elem.addEventListener('start',function(event){
                event.target.className = "trafficLight green";
            })
            elem.addEventListener('stop',function(event){
                event.target.className = "trafficLight red";
            })
            elem.addEventListener('click',function(){
                clearInterval(elem.dataset.intId);
                elem.classList.remove('yellow');
                if(elem.className==='trafficLight red'){
                    elem.dispatchEvent(Start);
                }else{
                    elem.dispatchEvent(Stop);
                }
            })
            
        })
        buttonDo.addEventListener('click',function(){
            allTrafficlights.forEach(function(elem){
                elem.removeEventListener('night',nightMode);
                elem.className='trafficLight';
                clearInterval(elem.dataset.intId);
                elem.addEventListener('night',nightMode);
                elem.dataset.intId = setInterval(function(){
                    elem.dispatchEvent(Night);
                },1000)
                
                
            })

        })
        
        
    
      });
   
  
  
  }; // custom events end!
  
  export default TrafficEvents;
