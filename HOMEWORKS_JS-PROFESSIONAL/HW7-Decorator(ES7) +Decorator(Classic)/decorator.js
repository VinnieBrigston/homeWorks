/*

  Задание: используя паттерн декоратор, модифицировать класс Human из примера basicUsage.

  0.  Создать новый конструктор, который будет принимать в себя человека как аргумент,
      и будем добавлять ему массив обьектов coolers (охладители), а него внести обьекты
      например мороженное, вода, сок и т.д в виде: {name: 'icecream', temperatureCoolRate: -5}

  1.  Расширить обработку функции ChangeTemperature в прототипе human таким образом,
      что если темпаретура становится выше 30 градусов то мы берем обьект из массива coolers
      и "охлаждаем" человека на ту температуру которая там указана.

      Обработку старого события если температура уходит вниз поставить с условием, что температура ниже нуля.
      Если температура превышает 50 градусов, выводить сообщение error -> "{Human.name} зажарился на солнце :("

  2.  Бонус: добавить в наш прототип нашего нового класса метод .addCooler(), который
      будет добавлять "охладитель" в наш обьект. Сделать валидацию что бы через этот метод
      нельзя было прокинуть обьект в котором отсутствует поля name и temperatureCoolRate.
      Выводить сообщение с ошибкой про это.

*/

const BeachParty = () => {

    function Human( name ){
        this.name = name;
        this.currentTemperature = 0;
        this.minTemperature = 0;
        this.maxTemperature = 50;

        console.log(`new Human ${this.name} arrived!`);
    }
    Human.prototype.ChangeTemperature = function ChangeTemperature( changeValue ){
        console.log(
          'current', this.currentTemperature + changeValue,
          'min', this.minTemperature
        );

        this.currentTemperature = this.currentTemperature + changeValue;

        if( this.currentTemperature < this.minTemperature ){
          console.error(`too cool!`);
        } else if(this.currentTemperature > 30){
          console.log(`It is necessary to cool down immediately or ${this.name} to melt!`);
        } else if(this.currentTemperature >= this.maxTemperature){
            console.error(`oh shit! ${this.name} melted `);
        }
    };

    function CoolHuman( Human ){
        this.name = Human.name;
        this.coolers = [
          { name: 'icescream', temperatureResistance: -15},
          { name: 'icetea', temperatureResistance: -10},
          { name: 'coldwater', temperatureResistance: -5},
        ];
        this.currentTemperature = 0;
        this.minTemperature = Human.minTemperature - this.coolers.reduce(
            (currentResistance, cooler ) => {
              console.log('currentResistance', currentResistance,  'clothe', cooler);
              return currentResistance + cooler.temperatureResistance;
            }, 0
          );
        console.log(`new Human ${this.name} arrived! He can survive in temperature ${this.minTemperature}`);
      }
      CoolHuman.prototype = Human.prototype;

      let Dexter = new CoolHuman( new Human('Dexter') );
          Dexter.ChangeTemperature(5);
          Dexter.ChangeTemperature(10);
          Dexter.ChangeTemperature(15);
          
}

export default BeachParty;
