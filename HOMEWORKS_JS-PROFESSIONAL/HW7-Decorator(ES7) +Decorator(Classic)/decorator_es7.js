/*

  Задание:
    1. Используя функциональный декоратор, написать декоратор который будет показывать
       аргументы и результат выполнения функции.

    2. Написать декоратор для класса, который будет преобразовывать аргументы в число,
       если они переданы строкой, и выводить ошибку если переданая переменная не
       может быть преобразована в число
*/

const Work1 = () => {
  function decorate( target, key, descriptor ){
    const originFn = descriptor.value.bind(this);
    var index = 0;
    descriptor.value = function(...args){
      let id = index++;
      console.log('arguments-'+args, 'index', index); 
      console.time( key +' - ' + id );
      let value = originFn( ...args );
      console.timeEnd( key +' - ' + id);
      return value;
    }
  }

  class CoolMath {
    @decorate
    addNumbers(a,b){ return a+b; }
    @decorate
    multiplyNumbers(a,b){ return a*b}
    @decorate
    minusNumbers(a,b){ return a-b }
  }
  let Calcul = new CoolMath();
  let x = Calcul.addNumbers(2, 2)
  let t = Calcul.addNumbers(2, 2)
  let y = Calcul.multiplyNumbers(10, 2)
  let z = Calcul.minusNumbers(10, 2)
  console.log(x, y, z);
};

export default Work1;
