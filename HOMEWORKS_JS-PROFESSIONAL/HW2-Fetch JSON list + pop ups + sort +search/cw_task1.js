/*

  Данные: http://www.json-generator.com/api/json/get/ceRHciXcVu?indent=2
  Задача.

  1.  Получить данные и в виде простой таблички вывести список компаний. Для начала используем поля:
      Company | Balance | Registered | Показать адресс | Кол-во employers | показать сотрудников

  2.  Сделать сортировку таблицы по количеству сотрудников и балансу. Сортировка должна происходить по клику
      на заголовок столбца

  3.  По клику на показать адресс должна собиратся строка из полей адресса и показываться на экран.

  4.  По клику на показать сотрудников должна показываться другая табличка формата:

      <- Назад к списку компаний | *Название компании*
      Сотрудники:
      Name | Gender | Age | Contacts

  5.  В второй табличке долен быть реализован поиск сотрудников по их имени, а так же сортировка по
      полу и возрасту.

  Примечание: Весь код должен писатся с учетом синтаксиса и возмжность ES6.

*/
let container = document.getElementById('table'),
    popUp = document.getElementById('popUp'),
    close = document.getElementById('closePopUp'),
    popUpBody = document.getElementById('popUpBody'),
    overLay = document.getElementById('overLay'),
    balanceButton = document.getElementById('balance'),
    popUpHeader = document.getElementById('popUpHeader');
let searchInput = document.createElement('input');



let list = fetch('http://www.json-generator.com/api/json/get/ceRHciXcVu?indent=2')
    .then(
        function(response) {
            // Examine the text in the response
            response.json().then(constructTable);
        }
    );
let clearTable = () => {
    let oldItems = popUpBody.childNodes,
        arrayOldItems = Array.from(oldItems);
    arrayOldItems.forEach(function (element) {
        popUpBody.removeChild(element);
    });
}
let output = (array) => {
    clearTable();
    array.forEach(function(elem){
        let onePerson = document.createElement('div'),
            aboutPerson = addPerson(elem);
        onePerson.className = 'one-person';
        onePerson.innerHTML = aboutPerson;
        popUpBody.appendChild(onePerson);
    });
};
let searchEmployer = (employersArray) => (e) => {
    let value = e.target.value.toLowerCase(),
        resultSearch = employersArray.filter((item) => item.name.toLowerCase().match(value));

    output(resultSearch);
};
let closePopUp = () =>{
    popUp.classList.remove('show');
    overLay.classList.remove('show');
};
let showAdress = (item) => (event) => {
    clearTable();
    let adressObj = item.address;
    for (var key in adressObj) {
        let rowAdress = document.createElement('div');
        rowAdress.className = 'one-adress';
        rowAdress.innerHTML = `${key} : ${adressObj[key]}`;
        popUpBody.appendChild(rowAdress);

    }
    popUp.classList.add('show');
    overLay.classList.add('show');
};

let showEmployers = (item) => (event) => {
    let oldItems = popUpBody.childNodes,
        arrayOldItems = Array.from(oldItems);
    arrayOldItems.forEach(function(element){
        popUpBody.removeChild(element);
    });
    popUp.classList.add('show');
    overLay.classList.add('show');
    let employersArray = item.employers,
        searchInput = document.createElement('input'),
        existInput = popUpHeader.querySelector('input');
    searchInput.setAttribute('type','text');
    searchInput.setAttribute('placeholder','Search employer...');
    searchInput.addEventListener('input',searchEmployer(employersArray));
    output(employersArray);
    if(existInput){
        popUpHeader.removeChild(existInput);
    }
    popUpHeader.appendChild(searchInput);

};
let constructTable = (data) => {
    data.forEach(function(item){
        let oneRow = document.createElement('div'),
            button = document.createElement('button');
        button.className = 'show-employers';
        oneRow.className = 'one-row';
        let contentRow = addRow(item);
        oneRow.innerHTML = contentRow;
        // oneRow.addEventListener('click',showAdress);
        let buttonShowEmployers = oneRow.querySelector('.show-employers'),
            buttonShowAdress = oneRow.querySelector('.show-adress');
        buttonShowEmployers.addEventListener('click', showEmployers(item));
        buttonShowAdress.addEventListener('click', showAdress(item));
        container.appendChild(oneRow);
    })
};
let sortByBalance = () => {
    fetch('http://www.json-generator.com/api/json/get/ceRHciXcVu?indent=2')
        .then(
            function(response) {
                // Examine the text in the response
                response.json().then(function(data){
                    let sortData = data.sort(function (a, b) {
                        if (a.balance > b.balance) {
                            return -1;
                        }
                        if (a.balance < b.balance) {
                            return 1;
                        }
                        // a должно быть равным b
                        return 0;
                    });
                    let allRows = document.querySelectorAll('.one-row');
                    allRows.forEach(function(item){
                        container.removeChild(item);
                    });
                    constructTable(sortData);
                });
            }
        );
};
function addRow(item){
    return `
            <div class="company">${item.company}</div>
            <div class="balance">${item.balance}</div>
            <div class="registered">${item.registered}</div>
            <div class="employers-number">${item.employers.length} <button class="show-employers">show all</button></div>
            <div class="company-adress"><span class="show-adress">show adress</span></div>
          
        `;
};
function addAdress(elem){
    return `
           
            <div class="city">City: ${elem.city}</div>
            <div class="zip">Zip: ${elem.zip}</div>
            <div class="country">Country: ${elem.country}</div>
            <div class="state">State: ${elem.state}</div>
            <div class="street">Street: ${elem.street}</div>
            <div class="house">House: ${elem.house}</div>
          
        `;
};
function addPerson(elem){
    return `
           
            <div class="name">${elem.name}</div>
            <div class="gender">${elem.gender}</div>
            <div class="age">${elem.age}</div>
          
        `;
};





close.addEventListener('click',closePopUp);
balanceButton.addEventListener('click',sortByBalance);