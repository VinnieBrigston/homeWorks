
  /*

    Задание 1.

    Написать скрипт который будет будет переключать вкладки по нажатию
    на кнопки в хедере.

    Главное условие - изменять файл HTML нельзя.

    Алгоритм:
      1. Выбрать каждую кнопку в шапке
         + бонус выбрать одним селектором

      2. Повесить кнопку онклик
          button1.onclick = function(event) {

          }
          + бонус: один обработчик на все три кнопки

      3. Написать функцию которая выбирает соответствующую вкладку
         и добавляет к ней класс active

      4. Написать функцию hideAllTabs которая прячет все вкладки.
         Удаляя класс active со всех вкладок

    Методы для работы:

      getElementById
      querySelector
      classList
      classList.add
      forEach
      onclick

      element.onclick = function(event) {
        // do stuff ...
      }

  */

  /* Решение*/

  /* Выбираем все кнопки на странице */
  var buttons = document.querySelectorAll('button');


  /* Перебираем кнопки методом forEach. Берем с каждой кнопки data-tab и по значению
     находим нужную панель и добавляем ей класс active
  */
  buttons.forEach( function( item ){
      item.onclick = function(event) {
          var tabId = item.dataset.tab;
          var tab = document.querySelector('div.tab[data-tab="'+tabId+'"]');
          /* И затем просто этой панельке в этой же функии накидываем класс active*/
          tab.classList.add('active');
      };
  });

  /* создаем новую кнопку для чистки экрана, добавляем класс как и у всех кнопок, добавляем текст и закидываем в DOM */
  var  hideButton = document.createElement('button');

  var buttonContainer = document.getElementById('buttonContainer');

  hideButton.classList.add('showButton');

  hideButton.innerHTML = 'Hide All Buttons';

  buttonContainer.appendChild(hideButton);


  /* Создаем фунцию для удаления класса active с панелек */
  var hideAllTabs = function(){
      var allTabs = document.querySelectorAll('.tab');
      allTabs.forEach(function(item){
          item.classList.remove('active');
      });
  };

  /* Вешаем функцию на клик по новой кнопке */
  hideButton.onclick = hideAllTabs;




